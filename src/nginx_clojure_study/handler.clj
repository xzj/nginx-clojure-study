(ns nginx-clojure-study.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.response :refer (Renderable)]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [nginx.clojure.core :refer (phase-done)]
            ))

(extend-protocol Renderable
                 java.util.Map (render [resp _] resp)
                 )

(defroutes app-routes
  (GET "/a/:file" [] phase-done)
  (route/not-found {:status 401
                    :headers {"Content-Type" "text/plain; charset=utf-8"}
                     :body "Unauthorized"}))

(def app
  (wrap-defaults app-routes site-defaults))
